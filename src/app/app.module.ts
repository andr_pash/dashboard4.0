import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WcContainerComponent } from './wc-container/wc-container.component';
import { WcRegistryService } from './wc-registry.service';
import { SideBarComponent } from './side-bar/side-bar.component';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { ContentAreaComponent } from './content-area/content-area.component';

@NgModule({
  declarations: [
    AppComponent,
    WcContainerComponent,
    SideBarComponent,
    TitleBarComponent,
    ContentAreaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [WcRegistryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
