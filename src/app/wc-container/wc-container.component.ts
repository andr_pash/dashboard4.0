import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { StoreService } from '../store.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-wc-container',
  templateUrl: './wc-container.component.html',
  styleUrls: ['./wc-container.component.css']
})
export class WcContainerComponent implements OnInit, AfterViewInit, OnDestroy {

  private _componenName: string;
  private currentNode: Node;

  @ViewChild('anchor') anchor: ElementRef;

  @Input()
  set componentName(name: string) {
    this._componenName = name;
    this.renderComponent();
  };

  get componentName(): string {
    return this._componenName;
  }

  get nativeElement(): HTMLElement {
    return this.anchor.nativeElement;
  }

  private _sub: Subscription;

  constructor(private store: StoreService) {
    this._sub = this.store.message$.subscribe(message => {
      if (this.currentNode) {
        this.currentNode['registerStateChange'](message)
      }
    })
  }

  ngOnInit() {}

  ngOnDestroy() {
    this._sub.unsubscribe()
  }

  renderComponent() {
    const component = document.createElement(this.componentName);

    if (this.currentNode) {
      this.nativeElement.removeChild(this.currentNode)
    }

    this.currentNode = this.nativeElement.appendChild(component);
  }

  async ngAfterViewInit() {

  }
}
