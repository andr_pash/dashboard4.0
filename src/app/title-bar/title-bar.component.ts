import { Component, OnInit } from '@angular/core';
import { StoreService } from '../store.service';
import { WcRegistryService } from '../wc-registry.service';

@Component({
  selector: 'app-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.css']
})
export class TitleBarComponent implements OnInit {

  constructor(private registry: WcRegistryService, public store: StoreService) { }

  ngOnInit() {
  }

}
