import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentAreaComponent } from './content-area/content-area.component';

const routes: Routes = [
  { path: ':component', component: ContentAreaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
