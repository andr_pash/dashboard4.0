import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WcRegistryService {
  static KONSUL_URL =  'http://127.0.0.1:8080/microservices.json'

  uniqueId = 0

  uiRegistry: UIRegistry = {
    view_app: [],
    side_bar: []
  }

  constructor() {
    this.fetchUIInformation()
  }

  async fetchUIInformation() {
    const availableMicroserviceUrls: string[] = await fetch(WcRegistryService.KONSUL_URL).then(res => res.json())

    for (const url of availableMicroserviceUrls) {
      const uiConfig = await this.fetchMicroserviceConfig(url)

      this.registerUI(uiConfig)
    }
  }

  // TODO: use proper url
  async fetchMicroserviceConfig(url: string): Promise<UIConfig> {
    return fetch(url).then(res => res.json())
  }

  async registerUI(uiConfig: UIConfig) {
    const sidebarComponent = await this.fetchComponent(uiConfig.side_bar)
    const viewAppComponent = await this.fetchComponent(uiConfig.view_app)

    const id = this.uniqueId++
    this.registerAppComponent(viewAppComponent, id)
    this.registerSideBarComponent(sidebarComponent, id)
  }

  async fetchComponent(url): Promise<HTMLElement> {
    return url && window['System'].import(url)
  }

  registerAppComponent(component, id: number) {
    if (!component) {
      return
    }

    const name = `aom-appview${id}`
    self.customElements.define(name, component)
    this.uiRegistry.view_app.push({ name, component, service: id })
  }

  registerSideBarComponent(component, id: number) {
    if (!component) {
      return
    }

    const name = `aom-sidebar${id}`
    self.customElements.define(name, component)
    this.uiRegistry.side_bar.push({ name, component, service: id })
  }

}



interface UIConfig {
  view_app: string
  side_bar: string
}

interface UIRegistry {
  view_app: WebComponent[]
  side_bar: WebComponent[]
}

interface WebComponent {
  name: string
  service: number
  component: HTMLElement
}
