import { Component, OnInit } from '@angular/core';
import { WcRegistryService } from '../wc-registry.service';
import { StoreService } from '../store.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor(private registry: WcRegistryService, public store: StoreService) { }

  ngOnInit() {
  }

  get components() {
    return this.registry.uiRegistry.side_bar
  }

}
