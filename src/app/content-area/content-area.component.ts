import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap, filter } from 'rxjs/operators';
import { WcRegistryService } from '../wc-registry.service';

@Component({
  selector: 'app-content-area',
  templateUrl: './content-area.component.html',
  styleUrls: ['./content-area.component.css']
})
export class ContentAreaComponent implements OnInit, AfterViewInit {

  componentName$: Observable<string>
  constructor(private activatedRoute: ActivatedRoute, private registry: WcRegistryService) {
    this.componentName$ = this.activatedRoute.paramMap.pipe(
      map(paramMap => paramMap.get('component')),
      map(serviceId => this.registry.uiRegistry.view_app.find(wc => wc.service === Number(serviceId))),
      filter(wc => !!wc),
      map(wc => wc.name)
    )
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

  }

}
