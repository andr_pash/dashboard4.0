import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private _message$: Subject<Message>
  message$: Observable<Message>

  constructor() {
    this._message$ = new Subject()
    this.message$ = this._message$.asObservable()
  }

  changeApp(name: string) {
    const message = new Message(MessageType.APP_CHANGE, name)
    this._message$.next(message)
  }

  changeCustomer(name: string) {
    const message = new Message(MessageType.CUSTOMER_CHANGE, name)
    this._message$.next(message)
  }
}


class Message {
  constructor(public type: MessageType, public payload: any) {}
}

enum MessageType {
  APP_CHANGE = 'APP_CHANGE',
  CUSTOMER_CHANGE = 'CUSTOMER_CHANGE'
}
